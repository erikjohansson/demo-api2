FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 59634
EXPOSE 44365

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY DemoApi/DemoApi.csproj ./DemoApi/
RUN dotnet restore ./DemoApi/DemoApi.csproj
COPY . .
WORKDIR /src/DemoApi
RUN dotnet build DemoApi.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish DemoApi.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "DemoApi.dll"]
